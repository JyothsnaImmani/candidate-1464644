Test Run File:

Implemented 5 Features with Maven - cucumber - serenity - Junit Framework using Page Objects.
Used Rest API client for implementing API tests

Tests - Implemented

    Visa Requirements for Japan Nationals to study in UK,
    Visa requirements for Japan Nationals to visit UK,
    Visa Requirements for Russian Nationals who wants to visit but are not visiting family or partner in UK,
    Visa Requirements for Russian Nationals who are visiting family or partner and have Article 10 or Article 20 card ,
    Check Postcode -  get request to api.postcodes.io/postcodes/SW1P4JA,

CucumberTestSuite - Runner Class
Maven _ clean - verify is used to generate serenity reports

src/test/java/HomeOfficeTests - has all the Tests implemented 

Folder Structure  -  

    features  --   Gherkin cucumber scenarios
    stepdefinitions -- implementation of scenarios
    Navigate -  Default url Step Library and page object classes
    Pages - Step Libraries and page objects classes for scenarios implemented


1. Scenario - JapanStudyVisa.features
    
       Feature:  Visa requirements to study for Japan nationals
        Scenario: Check visa status for Japan
        Given I select the nationality of Japan
        And I select reason Study
        When I select I intend to stay for more than six months
        Then I submit the form and I will be informed I need to study in UK
   
   
        
  File Locations - 
         
         Step definition class - Japanstudyvisa
         Step Library class - J_StudyHome,
         Page Object Class -  J_SPage

2.    Scenario - JTouristvisa.feature

          Feature: Tourist Visa requirements for Japan nationals
         
          Scenario: Check visa requirements for japan nationals to  visit uk
          Given I provide the nationality of Japan
          And I select reason Tourism
          When I submit the form
          Then I will be informed I won't need a visa to come to uk
       
      File Location - 
      
          Step definition class - Japantouristvisa 
          Step Library class - J_TouristHome
          Page Object Class -  J_TouristPage

3. Scenario - Russiavisa.feature  (There are 2 scenarios implemented)

        Feature:  Tourism visa for Russians
        
        Scenario: Check visa requirements for Russians nationals
          Given I provide the nationality of Russia
          And I select reason tourism
          And I select I am not travelling or visiting a partner or family
          When I submit
          Then I will be informed I need a visa to come to UK
     
         Scenario: Check visa requirements for russian ations who has for Article 10 or article 20 card
           Given provide the nationality of Russia
           And select reason tourism
           And select I am travelling to visit a partner or family
           When select I have Article card and submit the form
           Then will be informed I will not need a visa to come to UK

     File Locations - 
     
        Step definition class - Russiavisa
        Step Library classes -  RussiavisaHome,R_VisaRequiredHome
        Page Object Class - RussiavisaPages (contains all the elements for 2 scenarios for russia)
     
     4. PostcodeAPI.feature 
             
            Feature: Check on postcode status
       
            The `/api/status` end point returns a status message to indicate that the resource is fetched successfully.
            Scenario: postcode status end-point check
            Given I enter url and postcode: and check the response

       File Locations - 
            
             Step definition class - post
             Step Library classes -  PostcodeStatus


Test Summary Report - 

"C:\Program Files\Java\jdk1.8.0_241\bin\java.exe" "-Dmaven.multiModuleProjectDirectory=C:\Users\jyothsna.immani\Downloads\Candidate 1464644" "-Dmaven.home=C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.1.2\plugins\maven\lib\maven3" "-Dclassworlds.conf=C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.1.2\plugins\maven\lib\maven3\bin\m2.conf" "-Dmaven.ext.class.path=C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.1.2\plugins\maven\lib\maven-event-listener.jar" "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.1.2\lib\idea_rt.jar=65160:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.1.2\bin" -Dfile.encoding=UTF-8 -classpath "C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.1.2\plugins\maven\lib\maven3\boot\plexus-classworlds-2.6.0.jar;C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2020.1.2\plugins\maven\lib\maven3\boot\plexus-classworlds.license" org.codehaus.classworlds.Launcher -Didea.version2020.1.2 verify
[INFO] Scanning for projects...
[INFO] 
[INFO] ------------------< net.serenitybdd:Candidate1464644 >------------------
[INFO] Building Serenity BDD project using Cucumber 1.0.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ Candidate1464644 ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory C:\Users\jyothsna.immani\Downloads\Candidate 1464644\src\main\resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ Candidate1464644 ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ Candidate1464644 ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 4 resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ Candidate1464644 ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 15 source files to C:\Users\jyothsna.immani\Downloads\Candidate 1464644\target\test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:3.0.0-M4:test (default-test) @ Candidate1464644 ---
[INFO] Tests are skipped.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ Candidate1464644 ---
[WARNING] JAR will be empty - no content was marked for inclusion!
[INFO] Building jar: C:\Users\jyothsna.immani\Downloads\Candidate 1464644\target\Candidate1464644-1.0.0-SNAPSHOT.jar
[INFO] 
[INFO] --- maven-failsafe-plugin:3.0.0-M4:integration-test (default) @ Candidate1464644 ---
[INFO] 
[INFO] -------------------------------------------------------
[INFO]  T E S T S
[INFO] -------------------------------------------------------
Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/JTouristvisa.feature
15:53:32.722 [pool-1-thread-1] INFO   - 

-------------------------------------------------------------------------------------
     _______. _______ .______       _______ .__   __.  __  .___________.____    ____ 
    /       ||   ____||   _  \     |   ____||  \ |  | |  | |           |\   \  /   / 
   |   (----`|  |__   |  |_)  |    |  |__   |   \|  | |  | `---|  |----` \   \/   /  
    \   \    |   __|  |      /     |   __|  |  . `  | |  |     |  |       \_    _/   
.----)   |   |  |____ |  |\  \----.|  |____ |  |\   | |  |     |  |         |  |     
|_______/    |_______|| _| `._____||_______||__| \__| |__|     |__|         |__|    
                                                                                     
 News and tutorials at http://www.serenity-bdd.info                                  
 Documentation at https://wakaleo.gitbooks.io/the-serenity-book/content/             
 Join the Serenity Community on Gitter: https://gitter.im/serenity-bdd/serenity-core 
 Serenity BDD Support and Training at http://serenity-bdd.info/#/trainingandsupport  
 Learn Serenity BDD online at https://www.serenitydojo.net                           
-------------------------------------------------------------------------------------


15:53:32.734 [pool-1-thread-1] INFO   - Test Suite Started: Tourist Visa requirements for Japan nationals

Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/JapanStudyvisa.feature
15:53:32.757 [pool-1-thread-1] INFO   - Test Suite Started: Visa requirements to study for Japan nationals

Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/PostcodeAPI.feature
15:53:32.762 [pool-1-thread-1] INFO   - Test Suite Started: Check on postcode status

Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/Russiavisa.feature
15:53:32.766 [pool-1-thread-1] INFO   - Test Suite Started: Tourism visa for Russians

[INFO] Running Tourist Visa requirements for Japan nationals
Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/JTouristvisa.feature
15:53:33.187 [pool-1-thread-1] INFO   - 
 _____ _____ ____ _____   ____ _____  _    ____ _____ _____ ____  
|_   _| ____/ ___|_   _| / ___|_   _|/ \  |  _ \_   _| ____|  _ \ 
  | | |  _| \___ \ | |   \___ \ | | / _ \ | |_) || | |  _| | | | |
  | | | |___ ___) || |    ___) || |/ ___ \|  _ < | | | |___| |_| |
  |_| |_____|____/ |_|   |____/ |_/_/   \_\_| \_\|_| |_____|____/ 

Check visa requirements for japan nationals to  visit uk(tourist-visa-requirements-for-japan-nationals;check-visa-requirements-for-japan-nationals-to--visit-uk)
-------------------------------------------------------------------
15:53:34.610 [pool-1-thread-1] INFO  n.s.c.w.d.ProvideNewDriver - Instantiating driver
Jul 21, 2020 3:53:33 PM org.openqa.selenium.remote.DesiredCapabilities chrome
INFO: Using `new ChromeOptions()` is preferred to `DesiredCapabilities.chrome()`
15:53:34.612 [pool-1-thread-1] INFO  n.s.c.w.d.ProvideNewDriver - Driver capabilities: Capabilities {acceptInsecureCerts: false, browserName: chrome, goog:chromeOptions: {args: [], extensions: []}, loggingPrefs: org.openqa.selenium.logging..., platform: ANY, version: }
Starting ChromeDriver 83.0.4103.39 (ccbf011cb2d2b19b506d844400483861342c20cd-refs/branch-heads/4103@{#416}) on port 5525
Only local connections are allowed.
Please see https://chromedriver.chromium.org/security-considerations for suggestions on keeping ChromeDriver safe.
ChromeDriver was started successfully.
Jul 21, 2020 3:53:38 PM org.openqa.selenium.remote.ProtocolHandshake createSession
INFO: Detected dialect: W3C
15:53:46.636 [pool-1-thread-1] INFO   - 
        __    _____ _____ ____ _____   ____   _    ____  ____  _____ ____  
  _     \ \  |_   _| ____/ ___|_   _| |  _ \ / \  / ___|/ ___|| ____|  _ \ 
 (_)_____| |   | | |  _| \___ \ | |   | |_) / _ \ \___ \\___ \|  _| | | | |
  _|_____| |   | | | |___ ___) || |   |  __/ ___ \ ___) |___) | |___| |_| |
 (_)     | |   |_| |_____|____/ |_|   |_| /_/   \_\____/|____/|_____|____/ 
        /_/                                                                

Check visa requirements for japan nationals to  visit uk
----------------------------------------------------------------------------
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 13.869 s - in Tourist Visa requirements for Japan nationals
[INFO] Running Visa requirements to study for Japan nationals
Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/JapanStudyvisa.feature
15:53:46.770 [pool-1-thread-1] INFO   - 
 _____ _____ ____ _____   ____ _____  _    ____ _____ _____ ____  
|_   _| ____/ ___|_   _| / ___|_   _|/ \  |  _ \_   _| ____|  _ \ 
  | | |  _| \___ \ | |   \___ \ | | / _ \ | |_) || | |  _| | | | |
  | | | |___ ___) || |    ___) || |/ ___ \|  _ < | | | |___| |_| |
  |_| |_____|____/ |_|   |____/ |_/_/   \_\_| \_\|_| |_____|____/ 

Check visa status for Japan(visa-requirements-to-study-for-japan-nationals;check-visa-status-for-japan)
-------------------------------------------------------------------
15:53:47.527 [pool-1-thread-1] INFO  n.s.c.w.d.ProvideNewDriver - Instantiating driver
15:53:47.528 [pool-1-thread-1] INFO  n.s.c.w.d.ProvideNewDriver - Driver capabilities: Capabilities {acceptInsecureCerts: false, browserName: chrome, goog:chromeOptions: {args: [], extensions: []}, loggingPrefs: org.openqa.selenium.logging..., platform: ANY, version: }
Test passed with visa required
15:54:07.748 [pool-1-thread-1] INFO   - 
        __    _____ _____ ____ _____   ____   _    ____  ____  _____ ____  
  _     \ \  |_   _| ____/ ___|_   _| |  _ \ / \  / ___|/ ___|| ____|  _ \ 
 (_)_____| |   | | |  _| \___ \ | |   | |_) / _ \ \___ \\___ \|  _| | | | |
  _|_____| |   | | | |___ ___) || |   |  __/ ___ \ ___) |___) | |___| |_| |
 (_)     | |   |_| |_____|____/ |_|   |_| /_/   \_\____/|____/|_____|____/ 
        /_/                                                                

Check visa status for Japan
----------------------------------------------------------------------------
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 21.111 s - in Visa requirements to study for Japan nationals
Jul 21, 2020 3:53:47 PM org.openqa.selenium.remote.DesiredCapabilities chrome
INFO: Using `new ChromeOptions()` is preferred to `DesiredCapabilities.chrome()`
Starting ChromeDriver 83.0.4103.39 (ccbf011cb2d2b19b506d844400483861342c20cd-refs/branch-heads/4103@{#416}) on port 28624
Only local connections are allowed.
Please see https://chromedriver.chromium.org/security-considerations for suggestions on keeping ChromeDriver safe.
ChromeDriver was started successfully.
Jul 21, 2020 3:53:51 PM org.openqa.selenium.remote.ProtocolHandshake createSession
INFO: Detected dialect: W3C
[INFO] Running Check on postcode status
Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/PostcodeAPI.feature
15:54:07.776 [pool-1-thread-1] INFO   - 
 _____ _____ ____ _____   ____ _____  _    ____ _____ _____ ____  
|_   _| ____/ ___|_   _| / ___|_   _|/ \  |  _ \_   _| ____|  _ \ 
  | | |  _| \___ \ | |   \___ \ | | / _ \ | |_) || | |  _| | | | |
  | | | |___ ___) || |    ___) || |/ ___ \|  _ < | | | |___| |_| |
  |_| |_____|____/ |_|   |____/ |_/_/   \_\_| \_\|_| |_____|____/ 

postcode status end-point check(check-on-postcode-status;postcode-status-end-point-check)
-------------------------------------------------------------------
{"status":200,"result":{"postcode":"SW1P 4JA","quality":1,"eastings":530005,"northings":178818,"country":"England","nhs_ha":"London","longitude":-0.128717,"latitude":51.493367,"european_electoral_region":"London","primary_care_trust":"Westminster","region":"London","lsoa":"Westminster 021C","msoa":"Westminster 021","incode":"4JA","outcode":"SW1P","parliamentary_constituency":"Cities of London and Westminster","admin_district":"Westminster","parish":"Westminster, unparished area","admin_county":null,"admin_ward":"Vincent Square","ced":null,"ccg":"NHS Central London (Westminster)","nuts":"Westminster","codes":{"admin_district":"E09000033","admin_county":"E99999999","admin_ward":"E05000646","parish":"E43000236","parliamentary_constituency":"E14000639","ccg":"E38000031","ccg_id":"09A","ced":"E99999999","nuts":"UKI32"}}}
15:54:09.062 [pool-1-thread-1] INFO   - 
        __    _____ _____ ____ _____   ____   _    ____  ____  _____ ____  
  _     \ \  |_   _| ____/ ___|_   _| |  _ \ / \  / ___|/ ___|| ____|  _ \ 
 (_)_____| |   | | |  _| \___ \ | |   | |_) / _ \ \___ \\___ \|  _| | | | |
  _|_____| |   | | | |___ ___) || |   |  __/ ___ \ ___) |___) | |___| |_| |
 (_)     | |   |_| |_____|____/ |_|   |_| /_/   \_\____/|____/|_____|____/ 
        /_/                                                                

postcode status end-point check
----------------------------------------------------------------------------
[INFO] Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 1.314 s - in Check on postcode status

5
 Scenarios (
5 passed
)
19
 Steps (
19 passed
)
1m
3.306s


[INFO] Running Tourism visa for Russians
Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/Russiavisa.feature
15:54:09.069 [pool-1-thread-1] INFO   - 
 _____ _____ ____ _____   ____ _____  _    ____ _____ _____ ____  
|_   _| ____/ ___|_   _| / ___|_   _|/ \  |  _ \_   _| ____|  _ \ 
  | | |  _| \___ \ | |   \___ \ | | / _ \ | |_) || | |  _| | | | |
  | | | |___ ___) || |    ___) || |/ ___ \|  _ < | | | |___| |_| |
  |_| |_____|____/ |_|   |____/ |_/_/   \_\_| \_\|_| |_____|____/ 

Check visa requirements for Russians nationals(tourism-visa-for-russians;check-visa-requirements-for-russians-nationals)
-------------------------------------------------------------------
15:54:09.337 [pool-1-thread-1] INFO  n.s.c.w.d.ProvideNewDriver - Instantiating driver
15:54:09.337 [pool-1-thread-1] INFO  n.s.c.w.d.ProvideNewDriver - Driver capabilities: Capabilities {acceptInsecureCerts: false, browserName: chrome, goog:chromeOptions: {args: [], extensions: []}, loggingPrefs: org.openqa.selenium.logging..., platform: ANY, version: }
15:54:22.281 [pool-1-thread-1] INFO   - 
        __    _____ _____ ____ _____   ____   _    ____  ____  _____ ____  
  _     \ \  |_   _| ____/ ___|_   _| |  _ \ / \  / ___|/ ___|| ____|  _ \ 
 (_)_____| |   | | |  _| \___ \ | |   | |_) / _ \ \___ \\___ \|  _| | | | |
  _|_____| |   | | | |___ ___) || |   |  __/ ___ \ ___) |___) | |___| |_| |
Jul 21, 2020 3:54:09 PM org.openqa.selenium.remote.DesiredCapabilities chrome
INFO: Using `new ChromeOptions()` is preferred to `DesiredCapabilities.chrome()`
Starting ChromeDriver 83.0.4103.39 (ccbf011cb2d2b19b506d844400483861342c20cd-refs/branch-heads/4103@{#416}) on port 33942
Only local connections are allowed.
Please see https://chromedriver.chromium.org/security-considerations for suggestions on keeping ChromeDriver safe.
ChromeDriver was started successfully.
Jul 21, 2020 3:54:12 PM org.openqa.selenium.remote.ProtocolHandshake createSession
INFO: Detected dialect: W3C
Jul 21, 2020 3:54:22 PM org.openqa.selenium.remote.DesiredCapabilities chrome
INFO: Using `new ChromeOptions()` is preferred to `DesiredCapabilities.chrome()`
Starting ChromeDriver 83.0.4103.39 (ccbf011cb2d2b19b506d844400483861342c20cd-refs/branch-heads/4103@{#416}) on port 17041
Only local connections are allowed.
Please see https://chromedriver.chromium.org/security-considerations for suggestions on keeping ChromeDriver safe.
ChromeDriver was started successfully.
Jul 21, 2020 3:54:25 PM org.openqa.selenium.remote.ProtocolHandshake createSession
INFO: Detected dialect: W3C
 (_)     | |   |_| |_____|____/ |_|   |_| /_/   \_\____/|____/|_____|____/ 
        /_/                                                                

Check visa requirements for Russians nationals
----------------------------------------------------------------------------
Feature from file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/src/test/java/HomeOfficeTests/features/Russiavisa.feature
15:54:22.291 [pool-1-thread-1] INFO   - 
 _____ _____ ____ _____   ____ _____  _    ____ _____ _____ ____  
|_   _| ____/ ___|_   _| / ___|_   _|/ \  |  _ \_   _| ____|  _ \ 
  | | |  _| \___ \ | |   \___ \ | | / _ \ | |_) || | |  _| | | | |
  | | | |___ ___) || |    ___) || |/ ___ \|  _ < | | | |___| |_| |
  |_| |_____|____/ |_|   |____/ |_/_/   \_\_| \_\|_| |_____|____/ 

Check visa requirements for russian ations who has for Article 10 or article 20 card(tourism-visa-for-russians;check-visa-requirements-for-russian-ations-who-has-for-article-10-or-article-20-card)
-------------------------------------------------------------------
15:54:22.539 [pool-1-thread-1] INFO  n.s.c.w.d.ProvideNewDriver - Instantiating driver
15:54:22.539 [pool-1-thread-1] INFO  n.s.c.w.d.ProvideNewDriver - Driver capabilities: Capabilities {acceptInsecureCerts: false, browserName: chrome, goog:chromeOptions: {args: [], extensions: []}, loggingPrefs: org.openqa.selenium.logging..., platform: ANY, version: }
15:54:35.717 [pool-1-thread-1] INFO   - 
        __    _____ _____ ____ _____   ____   _    ____  ____  _____ ____  
  _     \ \  |_   _| ____/ ___|_   _| |  _ \ / \  / ___|/ ___|| ____|  _ \ 
 (_)_____| |   | | |  _| \___ \ | |   | |_) / _ \ \___ \\___ \|  _| | | | |
  _|_____| |   | | | |___ ___) || |   |  __/ ___ \ ___) |___) | |___| |_| |
 (_)     | |   |_| |_____|____/ |_|   |_| /_/   \_\____/|____/|_____|____/ 
        /_/                                                                

Check visa requirements for russian ations who has for Article 10 or article 20 card
----------------------------------------------------------------------------
[INFO] Tests run: 2, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 26.655 s - in Tourism visa for Russians
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 5, Failures: 0, Errors: 0, Skipped: 0
[INFO] 
[INFO] 
[INFO] --- serenity-maven-plugin:2.2.0:aggregate (serenity-reports) @ Candidate1464644 ---
[INFO] Test results for 5 tests generated in 1.9 secs in directory: file:/C:/Users/jyothsna.immani/Downloads/Candidate%201464644/target/site/serenity/
[INFO] -----------------------------------------
[INFO]  SERENITY TESTS : SUCCESS
[INFO] -----------------------------------------
[INFO] | Tests executed         | 5
[INFO] | Tests passed           | 5
[INFO] | Tests failed           | 0
[INFO] | Tests with errors      | 0
[INFO] | Tests compromised      | 0
[INFO] | Tests pending          | 0
[INFO] | Tests ignored/skipped  | 0
[INFO] ------------------------ | --------------
[INFO] | Total Duration         | 1m
[INFO] | Fastest test took      | 1s 286ms
[INFO] | Slowest test took      | 20s 240ms
[INFO] -----------------------------------------
[INFO] 
[INFO] SERENITY REPORTS
[INFO]   - Full Report: file:///C:/Users/jyothsna.immani/Downloads/Candidate%201464644/target/site/serenity/index.html
[INFO] 
[INFO] --- maven-failsafe-plugin:3.0.0-M4:verify (default) @ Candidate1464644 ---
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  01:21 min
[INFO] Finished at: 2020-07-21T15:54:43+01:00
[INFO] ------------------------------------------------------------------------


Serenity report
0d6265ac1e81af67eeb925d2bf6404cdab25f2fe0dd15c740691a952c2205c4a.html



PostCode API -  Test Result 

{"status":200,"result":{"postcode":"SW1P 4JA","quality":1,"eastings":530005,"northings":178818,"country":"England","nhs_ha":"London","longitude":-0.128717,"latitude":51.493367,"european_electoral_region":"London","primary_care_trust":"Westminster","region":"London","lsoa":"Westminster 021C","msoa":"Westminster 021","incode":"4JA","outcode":"SW1P","parliamentary_constituency":"Cities of London and Westminster","admin_district":"Westminster","parish":"Westminster, unparished area","admin_county":null,"admin_ward":"Vincent Square","ced":null,"ccg":"NHS Central London (Westminster)","nuts":"Westminster","codes":{"admin_district":"E09000033","admin_county":"E99999999","admin_ward":"E05000646","parish":"E43000236","parliamentary_constituency":"E14000639","ccg":"E38000031","ccg_id":"09A","ced":"E99999999","nuts":"UKI32"}}}
StatusLine:HTTP/1.1 200 OK
ContentType:application/json; charset=utf-8

1 Scenarios (1 passed)
1 Steps (1 passed)
0m4.239s
