package HomeOfficeTests.Pages;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.annotations.Step;
import org.jruby.RubyProcess;
import org.junit.Assert;

public class PostcodeStatus {

    @Step
    public void currentStatus()

    {
        RestAssured.baseURI = "http://api.postcodes.io/postcodes";
        RequestSpecification Request = RestAssured.given();
        Response response = Request.get("/SW1P4JA");
        int StatusCode = response.statusCode();
        Assert.assertEquals(StatusCode,200);
        ResponseBody body = response.getBody();
        String bodyAsString = body.asString();
        System.out.println(bodyAsString);

        String Statusline = response.statusLine();
        System.out.println("StatusLine:" + Statusline);

        String ContentType = response.getContentType();
        System.out.println("ContentType:" +ContentType);

    }
}
