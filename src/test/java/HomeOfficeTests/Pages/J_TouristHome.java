package HomeOfficeTests.Pages;

import net.thucydides.core.annotations.Step;

public class J_TouristHome {

    J_TouristPage touristPage;


    @Step("Navigate to home page")
    public void open_homepage()
    {
        //t_home.open(); // it will open the application
        touristPage.Nav();
    }

    @Step("I select nationality of Japan and click on next")
    public void purpose() throws InterruptedException
    {
        touristPage.nationality_selection();
        touristPage.next_btn();

    }

    @Step("I select reason tourism")
    public void selectTourism()
    {
        touristPage.select_pupose();
        touristPage.btn_2();
    }

    @Step("Check the visa requirements")
    public void verifyT()
    {
        touristPage.Verify_message();

    }
}

