package HomeOfficeTests.Pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;
import java.lang.*;

public class J_SPage extends PageObject {

    public void Nav()throws InterruptedException
    {
        String title = getDriver().getTitle();
    }

    public void country_japan() throws InterruptedException
    {
        WebElementFacade Dropdown = $(By.id("response"));
        Dropdown.selectByVisibleText("Japan");
        Assert.assertTrue(Dropdown.getSelectedVisibleTextValue().equalsIgnoreCase("Japan"));
    }

    public void next()

    {
        $(By.cssSelector("#current-question > button")).submit();
    }

    public void reason()throws InterruptedException
    {
        $(By.cssSelector("#current-question > div > div > fieldset > div.govuk-radios > div:nth-child(3) > label")).click();
        Thread.sleep(2000);
        $(By.cssSelector("#current-question > button")).submit();
    }

    public void duration()
    {
        $(By.xpath("//*[@id=\"current-question\"]/div/div/fieldset/div[2]/div[2]/label")).click();
    }
    public void submit()

    {
        $(By.cssSelector("#current-question > button")).submit();
    }


    public void req_notification()throws InterruptedException
    {
        String current_url = getDriver().getCurrentUrl();
        Assert.assertTrue(getTitle().contains("Check if you need a UK visa"));
        Thread.sleep(5000);
        //String path = $(By.xpath("//*[@id=\"result-info\"]/div[2]/h2"));
        //String path = $(By.cssSelector("#result-info > div.govuk-\\!-margin-bottom-6.result-body > h2"));
        //Assert.assertTrue($(By.cssSelector("#result-info > div.govuk-\\!-margin-bottom-6.result-body > h2")));


        System.out.println("Test passed with visa required");
    }}
