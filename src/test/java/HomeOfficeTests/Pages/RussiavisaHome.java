package HomeOfficeTests.Pages;

import HomeOfficeTests.navigation.NavigateTo;
import net.thucydides.core.annotations.Step;

public class RussiavisaHome {

    RussiavisaPages RPage1;



    @Step("Navigate to home page")
    public void nav_1()
    {
        RPage1.RNav();

    }

    @Step("Select russia nationality")
    public void  select_russia()
    {
        RPage1.Nationality();
        RPage1.Next_RBtn1();
    }

    @Step("select Travel Tourism")
    public void I_Select_tourism()
    {
        RPage1.select_tourism();
        RPage1.Next_RBtn2();
    }

    @Step("I select visiting family or leisure")
    public void  family_leisure_selection_Yes()
    {
        RPage1.VisitFamily_Yes();
        RPage1.Next_RBtn3();

    }


    @Step("I select I have ariticle 10 or article 20 card ")
    public void Article_Card_Yes()
    {
        RPage1.article1_yes();
        RPage1.Next_RBtn3();

    }


    @Step("check visa requirements")
    public void Verification()
    {
        RPage1.verification();
    }


}

