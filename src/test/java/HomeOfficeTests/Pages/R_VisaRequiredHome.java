package HomeOfficeTests.Pages;

import net.thucydides.core.annotations.Step;

public class R_VisaRequiredHome {

    RussiavisaPages Page_R;

    @Step
    public void R_Nav1()
    {
        Page_R.RNav();
    }

    @Step
    public void Nationality_R()
    {

        Page_R.Nationality();
        Page_R.Next_RBtn1();
    }

    @Step("I select tourism")
    public void select_tourism()
    {
        Page_R.select_tourism();
        Page_R.Next_RBtn2();
    }
    @Step
    public void Leisure_No()
    {
        Page_R.VisitFamily_No();
    }

    @Step
    public void lesirure_Button()
    {
        Page_R.Next_RBtn3();

    }

    @Step
    public void RequireVisa()
    {
        Page_R.verification();

    }
}
