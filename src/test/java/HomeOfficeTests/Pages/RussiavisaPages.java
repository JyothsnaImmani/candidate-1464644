package HomeOfficeTests.Pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class RussiavisaPages extends PageObject {


    public void RNav()
    {

       String title = getDriver().getTitle();
    }

    public void Nationality()
    {
        WebElementFacade Dropdown2 = $(By.id("response"));
        Dropdown2.selectByVisibleText("Russia");
        Assert.assertTrue(Dropdown2.getSelectedVisibleTextValue().equalsIgnoreCase("Russia"));

    }

    public void Next_RBtn1()
    {
        $(By.cssSelector("#current-question > button")).submit();

    }

    public void select_tourism()
    {
        $(By.cssSelector("#current-question > div > div > fieldset > div.govuk-radios > div:nth-child(1) > label")).click();
    }

    public void Next_RBtn2()
    {
        $(By.xpath("//*[@id=\"current-question\"]/button")).submit();
    }

    public void VisitFamily_Yes()
    {
    $(By.id("response-0")).click();

    }

    public void VisitFamily_No()
    {
        $(By.id("response-1")).click();
    }

    public void Next_RBtn3()
    {
   $(By.cssSelector("#current-question > button")).submit();
    }

    public void article1_yes()
    {
        $(By.xpath("//*[@id=\"current-question\"]/div/div/fieldset/div[3]/div[1]/label")).click();

    }
    public void article_no()
    {
        $(By.xpath("//*[@id=\"current-question\"]/div/div/fieldset/div[3]/div[2]/label")).click();
    }

    public void Next_RBtn4()
    {
        $(By.cssSelector("#current-question > button")).submit();
    }
    public void verification()
    {
        String url1 = getDriver().getTitle();
        Assert.assertTrue(getTitle().contains("Check if you need a UK visa"));
    }



}
