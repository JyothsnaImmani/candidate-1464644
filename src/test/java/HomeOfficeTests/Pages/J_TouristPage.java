package HomeOfficeTests.Pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

public class J_TouristPage extends PageObject {

    public void  Nav()
    {

        String title = getDriver().getTitle();
    }

    public void nationality_selection()
    {

        WebElementFacade Dropdown1 = $(By.id("response"));
        Dropdown1.selectByVisibleText("Japan");
        Assert.assertTrue(Dropdown1.getSelectedVisibleTextValue().equalsIgnoreCase("Japan"));

    }
    public void next_btn()
    {

        $(By.cssSelector("#current-question > button")).submit();
    }

    public void select_pupose()
    {
        $(By.cssSelector("#current-question > div > div > fieldset > div.govuk-radios > div:nth-child(1) > label")).click();

    }
    public void btn_2()
    {
        $(By.cssSelector("#current-question > button")).submit();
    }

    public void Verify_message()
    {
        String url = getDriver().getCurrentUrl();
        Assert.assertTrue(getTitle().contains("Check if you need a UK visa"));
    }


}


