package HomeOfficeTests.Pages;

import net.thucydides.core.annotations.Step;

public class J_StudyHome {

    J_SPage JS;



    @Step("Navigate to Gov.uk page")
    public void Navigation() throws InterruptedException
    {

        JS.Nav();
    }

    @Step("I select nationality of Japan and click on next")
    public void select_japan() throws InterruptedException
    {
        JS.country_japan();
        JS.next();
    }

    @Step("Select reason Study")
    public void select_study()throws InterruptedException
    {
        JS.reason();

    }

    @Step("Select study duration")
    public void study_duration()
    {
        JS.duration();
        JS.submit();
    }

    @Step("Verify Visa Requirements")
    public void verify() throws InterruptedException
    {

        JS.req_notification();

    }
}
