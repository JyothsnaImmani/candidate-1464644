package HomeOfficeTests.navigation;

import net.thucydides.core.annotations.Step;

public class NavigateTo {

    GovPage Govpage;

    @Step("Open the gov.uk home page")
    public void GovPage() {
        Govpage.open();
    }
}
