package HomeOfficeTests.Runner;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;
import HomeOfficeTests.Pages.*;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        strict = true,
        features = {"src/test/java/HomeOfficeTests/features"}, glue = {"HomeOfficeTests/stepdefinitions","HomeOfficeTests/navigation","HomeOfficeTests/Pages"}
        // ,"test/navigation","test/Pages"}

       // features = {"src/test/java/HomeOfficeTests/features/postcodeapi.feature"}, glue = {"HomeOfficeTests/stepdefinitions","HomeOfficeTests/APIClasses"}

)
public class CucumberTestSuite {}
