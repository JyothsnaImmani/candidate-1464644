# new feature
# Tags: optional

Feature:  Tourist Visa requirements for Japan nationals
  Scenario: Check visa requirements for japan nationals to  visit uk
    Given I provide the nationality of Japan
    And I select reason Tourism
    When I submit the form
    Then I will be informed I won't need a visa to come to uk
