# new feature
# Tags: optional

Feature:  Tourism visa for Russians
  Scenario: Check visa requirements for Russians nationals who are not visiting family or partner
    Given I provide the nationality of Russia
    And I select reason tourism
    And I select I am not travelling or visiting a partner or family
    When I submit
    Then I will be informed I need a visa to come to UK

    Scenario: Check visa requirements for russian nationals who has for Article 10 or article 20 card to visit family
      Given provide the nationality of Russia
      And select reason tourism
      And select I am travelling to visit a partner or family
      When select I have Article card and submit the form
      Then will be informed I will not need a visa to come to UK
