# new feature
# Tags: optional
Feature:  Visa requirements to study for Japan nationals
  Scenario: Check visa status for Japan
    Given I select the nationality of Japan
    And I select reason Study
    When I select I intend to stay for more than six months
    Then I submit the form and I will be informed I need to study in UK