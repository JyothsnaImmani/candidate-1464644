package HomeOfficeTests.stepdefinitions;

import HomeOfficeTests.Pages.R_VisaRequiredHome;
import HomeOfficeTests.Pages.RussiavisaHome;
import HomeOfficeTests.Pages.RussiavisaPages;
import HomeOfficeTests.navigation.NavigateTo;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class Russiavisa {

    @Steps
    RussiavisaHome R_home;


    @Steps
    R_VisaRequiredHome RVHome;

    @Steps
    NavigateTo  Nav_gov;

    @Managed
    WebDriver driver;

    @Given("I provide the nationality of Russia")
    public void iProvideTheNationalityOfRussia()
    {
        Nav_gov.GovPage();
        RVHome.R_Nav1();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        RVHome.Nationality_R();

    }

    @And("I select reason tourism")
    public void iSelectReasonTourism()
    {
        RVHome.select_tourism();

    }

    @And("I select I am not travelling or visiting a partner or family")
    public void iSelectIAmNotTravellingOrVisitingAPartnerOrFamily()
    {
        RVHome.Leisure_No();

    }

    @When("I submit")
    public void iSubmit()
    {
        RVHome.lesirure_Button();

    }

    @Then("I will be informed I need a visa to come to UK")
    public void iWillBeInformedINeedAVisaToComeToUK()
    {
        RVHome.RequireVisa();
        driver.close();
    }


    @Given("provide the nationality of Russia")
    public void provideTheNationalityOfRussia()
    {
        Nav_gov.GovPage();
        R_home.nav_1();
        driver.manage().window().maximize();
        R_home.select_russia();


    }

    @And("select reason tourism")
    public void selectReasonTourism()
    {
        R_home.I_Select_tourism();
    }


    @And("select I am travelling to visit a partner or family")
    public void selectIAmTravellingToVisitAPartnerOrFamily()
    {
        R_home.family_leisure_selection_Yes();

    }

    @When("select I have Article card and submit the form")
    public void selectIHaveArticleCard()
    {
        R_home.Article_Card_Yes();

    }


    @Then("will be informed I will not need a visa to come to UK")
    public void willBeInformedIWillNotNeedAVisaToComeToUK()
    {
        R_home.Verification();
    }
}
