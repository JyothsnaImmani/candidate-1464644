package HomeOfficeTests.stepdefinitions;

import HomeOfficeTests.Pages.J_SPage;
import HomeOfficeTests.Pages.J_StudyHome;
import HomeOfficeTests.navigation.NavigateTo;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;

public class Japanstudyvisa {

    @Steps
    J_StudyHome JSHome;

    @Steps
    J_SPage JSPage;

    @Steps
    NavigateTo N1;

    @Managed
    WebDriver driver;

    @Given("I select the nationality of Japan")
    public void iSelectTheNationalityOfJapan() throws InterruptedException
    {
        N1.GovPage();
        JSHome.Navigation();
        driver.manage().window().maximize();
        JSHome.select_japan();

    }

    @And("I select reason Study")
    public void iSelectReasonStudy() throws InterruptedException
    {
        JSHome.select_study();

    }

    @When("I select I intend to stay for more than six months")
    public void iSelectIIntendToStayForMoreThanMonths()
        {
        JSHome.study_duration();

    }

    @Then("I submit the form and I will be informed I need to study in UK")
    public void iSubmitTheFormAndIWillBeInformedINeedToStudyInUK() throws InterruptedException
    {
        JSHome.verify();
    }
}
