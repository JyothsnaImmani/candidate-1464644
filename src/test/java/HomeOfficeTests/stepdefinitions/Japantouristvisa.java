package HomeOfficeTests.stepdefinitions;

import HomeOfficeTests.Pages.J_TouristHome;
import HomeOfficeTests.navigation.NavigateTo;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;

public class Japantouristvisa
{
    @Steps
    NavigateTo N_Gov;

    @Steps
    J_TouristHome J_tourist;

    @Managed
    WebDriver driver;


    @Given("I provide the nationality of Japan")
    public void iProvideTheNationalityOfJapan()
    {
        N_Gov.GovPage();
        driver.manage().window().maximize();
        J_tourist.open_homepage();

    }

    @And("I select reason Tourism")
    public void iSelectReasonTourism() throws InterruptedException
    {
        J_tourist.purpose();
    }

    @When("I submit the form")
    public void iSubmitTheForm()
    {
     J_tourist.selectTourism();
    }

    @Then("I will be informed I won't need a visa to come to uk")
    public void iWillBeInformedIWonTNeedAVisaToComeToUk()
    {
        J_tourist.verifyT();
    }
}
