package HomeOfficeTests.stepdefinitions;

import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;
import HomeOfficeTests.Pages.PostcodeStatus;



public class postcodeapi {

    @Steps
    PostcodeStatus p_status;

    @Given("I enter url and postcode: and check the response")
    public void iEnterUrlAndPostcodeAndCheckTheResponse()
    {
        p_status.currentStatus();
    }
}
